import {clientSingletonCode} from "./patterns/creational/singleton";


export class Patterns {
  constructor() {
  }

  singletonPattern() {
    clientSingletonCode()
  }
}

const patters = new Patterns()

patters.singletonPattern()
