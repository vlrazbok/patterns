/**
 * The Singleton class defines the `getInstance` method that lets clients access
 * the unique singleton instance.
 */
export class Singleton {

  private static instance: Singleton;

  /**
   * The Singleton's constructor should always be private to prevent direct
   * construction calls with the `new` operator.
   */
  private constructor() {
  }

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton()
    }

    return Singleton.instance;
  }

  public someBusinessLogic() {
    console.log('One of function with business logic')
  }
}

export function clientSingletonCode() {
  const s1 = Singleton.getInstance();
  const s2 = Singleton.getInstance();

  if (s1 === s2){
    console.log('Singleton works, both variables contain the same instance.');
    s1.someBusinessLogic()
  } else {
    console.log('Singleton failed, variables contain different instances.');
  }

}
